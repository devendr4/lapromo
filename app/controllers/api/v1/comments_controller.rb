class Api::V1::CommentsController < ApplicationController
    skip_before_action :verify_authenticity_token

    # Retorna los comentarios pendientes
    def pending_comment
        @user = User.where(user_name: params[:id]).take

        @reservations = Reservation.no_comment(@user.diner.id.to_s)
    end

    def get_info
        @reservation = Reservation.find(params[:id]) 
    end


    def registrar_comment
        @error = {
            k_score:params[:kitchen_rating].nil?,
            s_score:params[:service_rating].nil?,
            a_score:params[:ambient_rating].nil?,
            description:params[:description].nil?,
            success:false
        }
        if(!@error[:k_score] && !@error[:s_score] && !@error[:a_score] && !@error[:description])
            
            @comment = Comment.new()
            @comment.kitchen_rating = params[:kitchen_rating]
            @comment.service_rating = params[:service_rating]
            @comment.ambient_rating = params[:ambient_rating]
            @comment.description = params[:description]
            @comment.reservation_id = params[:id]
            @comment.save(:validate => false)
            @error[:success] = true

            if (!params[:correo].nil?) 
                @reser = Reservation.find(params[:id])

                params[:correo].each {
                    |x|
                    CommentMailer.with(diner: @reser.diner, correo: x,promo:@reser.promo,comment:@comment).new_recommendation_mail.deliver_now

                }

            end
           
        end

    end


end
