class Api::V1::PromosController < ApplicationController


#Devuelve todas las promos para ser utilizadas en el filtro
  def promo_type_list
    @type = Promo.promo_type(params[:id])
     if @type.empty?
       render :json => {:error => "No existen promos disponibles"}.to_json, :status => 404
     else
       render json: @type
    end
  end

#Devuelve las promos de un restaurant
  def promo_restaurant
    @type = Promo.promos_list_of_restaurant(params[:id])
     if @type.empty?
       render :json => {:error => "No existen promos disponibles, para este restaurant"}.to_json, :status => 404
     else
       render json: @type
    end
  end

  def promo_dates_list
    @type = Promo.promo_dates_list(params[:id],params[:rest_id])
     if @type.empty?
       render :json => {:error => "No existen promos disponibles"}.to_json, :status => 404
     else
       render json: @type
    end
  end


  def promo_detail_list
    @type = Promo.list_of_promo_detail
     if @type.empty?
       render :json => {:error => "No existen promos disponibles"}.to_json, :status => 404
     else
       render json: @type
    end
  end

end
