class Comensal::HistYumisController < ApplicationController
  before_action :authenticate_user!, :permission_denied
  before_action :set_hist_yumi, only: [:show, :edit, :update, :destroy]

  # GET /hist_yumis
  # GET /hist_yumis.json
  def index
    @hist_yumis = HistYumi.all
  end
  ## METODOS PARA OBTENER HISTORIAL DE YUMIS TOTALES
  def mis_yumis
    @total_yumis = 0;
    @comensal= current_user.diner
    @comensal_yumis = HistYumi.yumis_personal(@comensal.id)
    if @comensal_yumis.length > 0
      @comensal_yumis.each do |yum|
        @total_yumis += yum.quantity
        flash[:success] = "Si hay yumis"
      end
    else
      flash[:warning] = "Usted no tiene puntos yumis"
    end
  end
  ## METODO PARA OBTENER HISTORIAL DE YUMIS POR INVITAR AMIGOS
  def yumis_amigos
    @total_yumis = 0;
    @comensal= current_user.diner
    @comensal_yumis = HistYumi.yumis_amigos(@comensal.id)
    if @comensal_yumis.length > 0
      @comensal_yumis.each do |yum|
        @total_yumis += yum.quantity
        flash[:success] = "Si hay yumis por invitar amigo"
      end
    else
      flash[:warning] = "Usted no tiene puntos yumis por invitar amigos"
    end
    @message = "por invitar amigos"
    render :mis_yumis
  end
  ## METODO PARA OBTENER HISTORIAL DE YUMIS DE RESERVAS EFECTUADAS
  def yumis_reservas_efectuadas
    @total_yumis = 0;
    @comensal= current_user.diner
    @comensal_yumis = HistYumi.yumis_reservas_efectuadas(@comensal.id)
    if @comensal_yumis.length > 0
      @comensal_yumis.each do |yum|
        @total_yumis += yum.quantity
        flash[:success] = "Si hay yumis por reservas efectuadas"
      end
    else
      flash[:warning] = "Usted no tiene puntos yumis por reservas efectuadas"
    end
    @message = "por reservas efectuadas"
    render :mis_yumis
  end
  ## METODO PARA OBTENER HISTORIAL DE YUMIS DE RESERVAS NO EFECTUADAS
  def yumis_reservas_noefectuadas
    @total_yumis = 0;
    @comensal= current_user.diner
    @comensal_yumis = HistYumi.yumis_reservas_noefectuadas(@comensal.id)
    if @comensal_yumis.length > 0
      @comensal_yumis.each do |yum|
        @total_yumis += yum.quantity
        flash[:success] = "Si hay yumis por reservas no efectuadas"
      end
    else
      flash[:warning] = "Usted no tiene puntos yumis por reservas no efectuadas"
    end
    @message = "por reservas no efectuadas"
    render :mis_yumis
  end
  ## METODO PARA OBTENER HISTORIAL DE YUMIS DE RESERVAS CANCELADAS
  def yumis_reservas_canceladas
    @total_yumis = 0;
    @comensal= current_user.diner
    @comensal_yumis = HistYumi.yumis_reservas_canceladas(@comensal.id)
    if @comensal_yumis.length > 0
      @comensal_yumis.each do |yum|
        @total_yumis += yum.quantity
        flash[:success] = "Si hay yumis por reservas canceladas"
      end
    else
      flash[:warning] = "Usted no tiene puntos yumis por reservas canceladas"
    end
    @message = "por reservas canceladas"
    render :mis_yumis
  end
  # GET /hist_yumis/1
  # GET /hist_yumis/1.json
  def show
  end

  # GET /hist_yumis/new
  def new
    @hist_yumi = HistYumi.new
  end

  # GET /hist_yumis/1/edit
  def edit
  end

  # POST /hist_yumis
  # POST /hist_yumis.json
  def create
    @hist_yumi = HistYumi.new(hist_yumi_params)

    respond_to do |format|
      if @hist_yumi.save
        format.html { redirect_to @hist_yumi, notice: 'Hist yumi was successfully created.' }
        format.json { render :show, status: :created, location: @hist_yumi }
      else
        format.html { render :new }
        format.json { render json: @hist_yumi.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hist_yumis/1
  # PATCH/PUT /hist_yumis/1.json
  def update
    respond_to do |format|
      if @hist_yumi.update(hist_yumi_params)
        format.html { redirect_to @hist_yumi, notice: 'Hist yumi was successfully updated.' }
        format.json { render :show, status: :ok, location: @hist_yumi }
      else
        format.html { render :edit }
        format.json { render json: @hist_yumi.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hist_yumis/1
  # DELETE /hist_yumis/1.json
  def destroy
    @hist_yumi.destroy
    respond_to do |format|
      format.html { redirect_to comensal_hist_yumis_url, notice: 'Hist yumi was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
   
  def permission_denied
      render plain: 'Debes ser usuario comensal para acceder a esta ruta.' if !current_user.is_diner?
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_hist_yumi
      @hist_yumi = HistYumi.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hist_yumi_params
      params.fetch(:hist_yumi, {})
    end
end
