class InvitationsController < ApplicationController
    skip_before_action :verify_authenticity_token
    before_action :authenticate_user!
    
    def new
        @invitation = Invitation.new
    end
    
    
    def create
       
        @invitation=Invitation.new(form_params) 
        @diner = current_user.id
        puts(@diner)
        @restaurant=params[:restID]
        @reservation=Reservation.find_by_sql("select res.id, r.id as rest,r.name as nombre ,r.address as direccion, d.name||' '|| d.last_name as comensal, 
                                             to_char(res.reservation_date, 'DD-MON-YYYY') as fecha,to_char(res.reservation_date, 'HH:MM') as hora,res.people_num as personas,res.diner_id as dine, p.promo_type as promocion
                                             from restaurants r,diners d,reservations res,users,promos p
                                             where  res.diner_id=d.id and r.id=#{@restaurant} and d.user_id=users.id and d.user_id=#{@diner}
                                             group by res.id,rest,nombre,direccion,comensal,fecha,personas,dine,promocion
                                             order by res.id DESC
                                             limit 1;")
       
            
        if @invitation.present?
                
            if @reservation.any?

                @reservation.each do |reserva|
                    
                     InviteFriendsMailer.with(invitation: @invitation,dinerID: reserva, restID: reserva.rest).invite_friend_mail.deliver_now
                     HistYumi.create(type_hist_yumis: 'Invitar amigo', quantity: 300, diner_id: reserva.dine)
                     redirect_to(restaurant_info_path(restaurantID:reserva.rest), notice:" Tu invitación al restaurant #{reserva.nombre} ha sido procesada satisfactoriamente, has recibido 300 Yummies Enhorabuena !!")

                end
                 
            else
                
                 redirect_to(restaurant_info_path(restaurantID:@restaurant), notice:" No hubo ninguna reserva realizada para este restaurant")
                
            end
                
        end
        
    end 
    
    


    
    private

    def form_params
       
        params.require(:invitation).permit(:email,:dinerID,:restID)

    end 



end
