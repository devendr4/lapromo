class ReservationsController < ApplicationController
    before_action :authenticate_user!, :permission_denied
    skip_before_action :verify_authenticity_token

    def index
        #PARA OBTENER LAS RESERVACIONES DE LOS COMENSALES QUE SE ENCUENTRA CON LA SESION ACTIVA
        #Y LO DE PAGE ES PARA MOSTRAR LA CONSULTA EN FORMA DE PAGINAS A SU VEZ LIMITANDO EL CONTENIDO A 6 OBJETOS POR PAGINA
        @dinerid = Diner.where(user_id:current_user.id).pluck('id')[0]
        @reservas = Reservation.where(diner_id:@dinerid ).where(status:'Pendiente').order('reservation_date ASC').page(params[:page]).per_page(5)
        #PARA OBTENER LA CANTIDAD DE RESERVACIONES DE LOS COMENSALES QUE SE ENCUENTRA CON LA SESION ACTIVA
        @contar = @reservas.count
        #ES PARA VALIDAR SI @RESERVA ESTA VACIA, Y SI ES ASI EMITE UNA NOTIFICACION AL USUARIO POR PANTALLA
        if @reservas.empty?
            flash[:pedro]= 'No posee reservaciones pendientes'
        end

        flash[:fecha] = 'Solo se puede cancelar con mas de 24 horas de antelacion'
        @reservaciones = Reservation.todas_reservaciones(@dinerid)
        #valida que haya reservaciones registradas
        if @reservaciones.empty?
            flash.now[:warning] = "No hay reservaciones registradas con el estado indicado"
        end
    end

    def cancelar
        @reserva = Reservation.where(id: params[:id]).where(diner_id: Diner.user(current_user.id)[0]).where(status:'Pendiente')
        if @reserva.empty?           
            @error = flash.now[:warning] = "Reserva inválida"
            render :empty
        end

    end

    def actualizando_cancel
        @reserva = Reservation.find(params[:id])
        @diner = Diner.find(@reserva.diner_id)
        if @reserva.update(status: 'cancelada')
            ReservationsMailer.with(diner: @diner, reservation: @reserva).cancel_reservation_email.deliver_now
            flash[:notice]= 'La reserva fue cancelada exitosamente'
            render json: {message: "La reserva fue cancelada exitosamente", id: Diner.user(current_user.id)[0]}, status: :ok
        else
            flash[:alert]= 'No se pudo cancelar la reserva'
            render json: {message: "No se pudo cancelar la reserva"}, status: :internal_server_error
        end

    end

    def permission_denied
        if ((!current_user.is_diner?))
            render :empty
        end
    end

    def new 
        @reservation = Reservation.new
    end


    #POST Creacion de reserva
    def create 
      
        @params1 = params[:reservation]
        @mes = @params1['fecha(2i)'].to_i
        @anio = @params1['fecha(1i)'].to_i
        @dia = @params1['fecha(3i)'].to_i
        @hour = @params1['hora(4i)'].to_i
        @min = @params1['hora(5i)'].to_i
        @user = current_user.id
        @restaurant = params[:restID]
        @promo = @params1[:promo].to_i
        @rest = Restaurant.find_by_sql("select r.* from restaurants r where r.id=#{@restaurant}")
        @diner = Diner.find_by_sql("select d.* from diners d where d.user_id=#{@user}")
        @usr = User.find_by_sql("select u.* from users u where u.id=#{@user}")
        @prom = Promo.find_by_sql("select p.* from promos p where p.id =#{@promo}")
        @reservation = Reservation.new(reservation_date: DateTime.new(@anio, @mes, @dia, @hour, @min, 0), people_num: @params1[:personas].to_i, diner_id: @diner[0].id, status: 'pendiente', promo_id: @promo.to_i, restaurant_id: @rest[0].id)

        if @reservation.save
            ReservationsMailer.with(reservation: @reservation, diner: @diner[0], restaurant: @rest[0], user: @usr[0], promo: @prom[0]).new_reservation_email.deliver_now
            redirect_to(restaurant_info_path(restaurantID: @restaurant), notice: "La reserva ha sido creada con exito")
        else
            redirect_to(restaurant_info_path(restaurantID:@restaurant), notice:"Hubo un error al crear la reserva")
        end
    end
    
    private 

    def form_params
        params.require(:reservation).permit(:fecha, :hora, :personas, :promo, :restID)
    end
end


