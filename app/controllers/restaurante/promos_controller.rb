class Restaurante::PromosController < ApplicationController
  before_action :set_promo, only: [:show, :edit, :update, :destroy]
  before_action  :set_promo_calendar, only: [:edit, :update, :destroy]
  before_action :authenticate_user!
  layout 'restaurante', only: %i[new create edit update index show]
  skip_before_action :verify_authenticity_token

  # GET /promos
  # GET /promos.json
  def index
    @promos = Promo.all
    @promo_calendars = PromoCalendar.all
    @calendars = Calendar.all
  end

  # GET /promos/1
  # GET /promos/1.json
  def show
    @promos = Promo.includes(:calendars).find(params[:id])
  end

  # GET /promos/new
  def new
    @promo = Promo.new
    @calendar = @promo.calendars.build
    @promo_calendar = @promo.promo_calendars.build
    @promo_calendar = PromoCalendar.new   # Calendario para guardar la fecha de la promoción
    @promo_plate = PromoPlate.new         # Plato en promoción dependiendo del tipo de promoción
  end

  # GET /promos/1/edit
  def edit
  end

  # POST /promos
  # POST /promos.json
  def create
    @promo = Promo.new(promo_params)

    @promo_plate = PromoPlate.new(promoplate_params)


    @restaurant = Restaurant.find_by user_id: current_user.id       #Se obtiene el ID del restaurante (usuario en sesión)
    @promo.restaurant_id = @restaurant.id

    respond_to do |format|
      if @promo.save
        format.html { redirect_to [:restaurante, @promo], notice: 'La promoción fue creada exitosamente.' }
        format.json { render :show, status: :created, location: @promo }
      else
        format.html { render :new }
        format.json { render json: @promo.errors, status: :unprocessable_entity }
      end
    end

    @promo_plate.promo_id = @promo.id                  # Guarda en la tabla promo_plate
    @promo_plate.plate_id = @promo.plate_ids           # el ID de la promo y del plato
    @promo_plate.save
  end

  # PATCH/PUT /promos/1
  # PATCH/PUT /promos/1.json
  def update
    respond_to do |format|
      if @promo.update(promo_params)
        format.html { redirect_to [:restaurante, @promo], notice: 'La promo fue actualizada exitosamente.' }
        format.json { render :show, status: :ok, location: @promo }
      else
        format.html { render :edit }
        format.json { render json: @promo.errors, status: :unprocessable_entity }
      end
      
      
    end

  end

  # DELETE /promos/1
  # DELETE /promos/1.json


  def destroy

    @promo.destroy

    respond_to do |format|
      format.html { redirect_to restaurante_promos_url, notice: 'La promo fue eliminada exitosamente.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_promo                             # Busca por ID de la promo antes de una acción
      @promo = Promo.find(params[:id])
    end
    def set_calendar                          # Busca el ID del calendario antes de una acción
      @calendar = Calendar.find(params[:id])
    end
    def set_promo_calendar                    # Busca por ID en promo_calendar antes de una acción
      @promo_calendar = PromoCalendar.find_by_promo_id(params[:promo_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.

    def promo_params                          # Define los parámetros de promo
      params.require(:promo).permit(:restaurant_id, :yumis, :promo_type, :name, :description, plates_attributes: [:id], calendars_attributes: [:final_date], promo_calendars_attributes: [:id, :promo_id, :calendar_id])
    end
    def promoplate_params                     # Define los parámetros de promoplate
      params.permit(:plate_id, :promo_id)
    end
    def calendar_params                          # Define los parámetros de promo
      params.require(:calendar).permit(:final_date, :final_date)
    end
    def promocalendar_params                  # Define los parámetros de promocalendar
      params.require(:promo).permit(:promo_id, :calendar_id)
    end
end