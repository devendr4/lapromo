class Restaurante::ReservationsController < ApplicationController
    include ApplicationHelper 
    layout 'restaurante'
    #before_action :authenticate_user!
    before_action :authenticate_user!, :check_for_restaurant_user, :set_restaurant
    skip_before_action :verify_authenticity_token


    def index
        @reservations = reservations_list()
    end

    def update_status
        reservation = Reservation.find(params[:id])
        reservation.status = params[:new_status].capitalize

        if (reservation.save)
            flash[:notice] = 'Se ha cambiado satisfactoriamente el status de la reserva'

            if (reservation.status == Reservation::USED)
                CommentRestaurantInviteMailer.with(reservation: reservation).send_invitation.deliver_now
            end
        else
            flash[:notice] = 'El status al que se intento cambiar la reserva es invalido'
        end

        redirect_to action: 'index'
    end

    
    #obtiene la lista de reservas que se han hecho en el restaurante
    def display_reservations 
        #@restaurantid = params[:restaurantid]
        #@dineridd = params[:dineridd]
        @reservaRest = Reservation.todas_rest(current_user.id)
    end

    def display_res_date 
        #
        
    end

    def show1 
        @search = params[:date]
        if @search 
            @reservaRestF = Reservation.todas_rest_f(current_user.id,params[:date])
            if @reservaRestF.empty? 
                @epa = flash[:warning] = "no se encontraron restaurantes en la fecha indicada"
            end
        end
    end

    def show2 
        #
    end

    private 
        def set_restaurant
            @restaurant =  Restaurant.find_by user_id: current_user.id  
        end
        
        def reservations_list
            @restaurant.reservations.order('reservation_date': :desc)
        end
    
end
