class RestaurantsController < ApplicationController
    helper_method :sort_column, :sort_direction
    
    
    def buscar_x_estado
        #Para la muestra de restaurantes por estado
        #recibe el id de un estado
        #luego revisa todos los municios asociados a ese estado
        #luego revisa todos las parroquias asociadas a a los municipios de ese estado
        #para devolver una lista de restaurantes que se encuentren asociados a esas esas parroquias
        #@restaurants = Restaurant.all #esta linea se uso para probar la conexion entre controlador y base de datos
        @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: (params[:place_id]))))).order(:name).group("restaurants.id")
        @@restaurants_estado = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: (params[:place_id]))))).order(:name).group("restaurants.id").pluck('id')
        redirect_to restaurants_buscador_path(option_busq: 'estados')

        if @restaurants.empty?
            flash.now[:warning] = "No hay restaurantes registrados en ese estado"
            #render :empty_search
        end
    end
    layout 'application'
    #INFO DEL RESTAURANT
    def restaurant_info
        @restaurantID = params[:restaurantID]
        #trae los menus del restaurant
        @menu = MenuType.find_by_sql("select menu_types.id, menu_types.name, plates.name as plate_name, plates.price, plates.description
            from menu_types, restaurants, plates
            where restaurants.id =#{@restaurantID} and restaurants.id = menu_types.restaurant_id and plates.menu_type_id = menu_types.id
            group by menu_types.id,  menu_types.name, plates.name, plates.price, plates.description")
        #obtiene los datos del restaurant
        @restaurant_info = Restaurant.find_by_sql("select id, name, address, map_location, zip_code, capacity, description,
                                                extract(HOUR from start_time) as hora_ap, extract(HOUR from end_time) as hora_cerr 
                                                from restaurants where restaurants.id =#{@restaurantID}")
        #obtiene los datos del promedio de todas las opiniones de ese restaurant
        @opinion_summarized = Comment.find_by_sql("select r.id as Restaurant,cast(avg(c.kitchen_rating) as decimal (10,1)) as cocina,cast(avg(c.service_rating) as decimal (10,1)) as servicio,cast(avg(c.ambient_rating) as decimal (10,1)) as ambiente,
                                                cast(avg(c.kitchen_rating + c.service_rating + c.ambient_rating)/3 as decimal (10,1)) as calificacion, count(c.id) as total_votos
                                                from restaurants r,diners p, comments c,reservations res
                                                where res.restaurant_id=r.id and res.diner_id=p.id and c.reservation_id=res.id and r.id=#{@restaurantID}
                                                group by Restaurant")

        if @restaurant_info.empty?
            flash.now[:error] = 'Este restaurante no existe, por favor vuelva a la página de inicio'
        end

        if @opinion_summarized.empty?
            flash.now[:error_o]= 'No hay calificaciones aún para este restaurant'
        end

        if @menu.empty?
            flash.now[:warning] = 'Este restaurante aun no posee un menú'
        end

    end

    def index
        @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: (params[:place_id])).pluck('id')).pluck('id')).pluck('id'))
    end
    #def index
    #@restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: (params[:place_id])).pluck('id')).pluck('id')).pluck('id'))

    def buscar_x_tipo_comida
        #Para la muestra de restaurantes por su tipo de comida asociado
        #recibe un texto para el tipo de comida
        #luego revisa las asociaciones de restaurantes con los tipos de comidas guardados
        #para devolver una lista de restaurantes cuyo tipo de comida es el indicado
        #@restaurantsT = Restaurant.all #esta linea se uso para probar la conexion entre controlador y base de datos
        @restaurants = Restaurant.where(id: RestaurantKitchen.where("id in (select restaurant_id from restaurant_kitchens where kitchen_type_id in (select id from kitchen_types where name like ? ))", params[:name] )).order(:name).group("restaurants.id").page(params[:page]).per_page(8)
        @@restaurants_tipos_comida = Restaurant.where(id: RestaurantKitchen.where("id in (select restaurant_id from restaurant_kitchens where kitchen_type_id in (select id from kitchen_types where name like ? ))", params[:name] )).order(:name).group("restaurants.id").pluck('id')
             redirect_to restaurants_buscador_path(option_busq: 'tipos_comida')   
        if @restaurants.empty?
            flash.now[:warning] = "No hay restaurantes registrados con ese tipo de comida"
            #render :empty_search
        end
    end

    def buscador
        @search = params[:title]
        @option_busq = params[:option_busq]
        @option = params[:opcion]
        @menus = MenuType.group(:name).pluck('name').sort!
        @plates = params[:plates_id]
        @selected_plates = (params[:plates_id].present? ? params[:plates_id] : [])
        @menor_rating = params[:menor_rating].to_i
        @mayor_rating = params[:mayor_rating].to_i
        @mayor_rating = 5 if @mayor_rating==0
        @menor_price = params[:menor_price].to_i
        @mayor_price = params[:mayor_price].to_i
        @mayor_price = 20 if @mayor_price == 0

        @estados = Place.find_by_sql("select id, name from places where type_place = 'estado'")
        @kitchen = KitchenType.all
       

        restaurants_base = Restaurant.search(@search).menu_type(@plates).rango_rating(@menor_rating,@mayor_rating).rango_price(@menor_price,@mayor_price)

        case @option_busq
           when 'estados' 
            restaurants_base = Restaurant.for_order(restaurants_base.pluck('id') & @@restaurants_estado).search(@search).menu_type(@plates).rango_rating(@menor_rating,@mayor_rating).rango_price(@menor_price,@mayor_price)
        when 'tipos_comida'
            restaurants_base = Restaurant.for_order(restaurants_base.pluck('id') & @@restaurants_tipos_comida).search(@search).menu_type(@plates).rango_rating(@menor_rating,@mayor_rating).rango_price(@menor_price,@mayor_price)
        when 'yumis'
            restaurants_base = Restaurant.for_order(restaurants_base.pluck('id') & @@restaurants_yumis).search(@search).menu_type(@plates).rango_rating(@menor_rating,@mayor_rating).rango_price(@menor_price,@mayor_price)
        when 'calif'
            restaurants_base = Restaurant.for_order(restaurants_base.pluck('id') & @@restaurants_calif).search(@search).menu_type(@plates).rango_rating(@menor_rating,@mayor_rating).rango_price(@menor_price,@mayor_price)
        when 'tipo_promo'
            restaurants_base = Restaurant.for_order(restaurants_base.pluck('id') & @@restaurants_promo).search(@search).menu_type(@plates).rango_rating(@menor_rating,@mayor_rating).rango_price(@menor_price,@mayor_price)
        when 'avanzada'
             restaurants_base = Restaurant.for_order(restaurants_base.pluck('id') & @@restaurants_avanzada).search(@search).menu_type(@plates).rango_rating(@menor_rating,@mayor_rating).rango_price(@menor_price,@mayor_price)
        when 'mapa'
            restaurants_base = Restaurant.for_order(restaurants_base.pluck('id') & @@restaurants_mapa).search(@search).menu_type(@plates).rango_rating(@menor_rating,@mayor_rating).rango_price(@menor_price,@mayor_price)

        else
            restaurants_base
        end
        #opciones para ordenar los resultados
        case @option
        when'plates_asc'
            @restaurants = Restaurant.plates_asc(restaurants_base.pluck('id')).page(params[:page]).per_page(6)
        when 'plates_desc'
            @restaurants = Restaurant.plates_desc(restaurants_base.pluck('id')).page(params[:page]).per_page(6)
        when 'ratings_desc'
            @restaurants = Restaurant.rating_desc(restaurants_base.pluck('id')).page(params[:page]).per_page(6)
        when'ratings_asc'
            @restaurants = Restaurant.rating_asc(restaurants_base.pluck('id')).page(params[:page]).per_page(6)
        when 'promo_desc'
            @restaurants = Restaurant.promo_desc(restaurants_base.pluck('id')).page(params[:page]).per_page(6)
        when 'comments_asc'
            @restaurants = Restaurant.comments_desc(restaurants_base.pluck('id')).page(params[:page]).per_page(6)
        when 'comments_desc'
            @restaurants = Restaurant.comments_asc(restaurants_base.pluck('id')).page(params[:page]).per_page(6)
        else
            @restaurants = restaurants_base.page(params[:page]).per_page(6)

        end
            @success = flash[:success] = "Se encontraron " + @restaurants.count.to_s + " restaurantes"

        if @restaurants.empty?
            # if @search.length > 1
            #     @msg = flash[:warning2] = "No se encontraron resultados para '" + @search+"'"
            # else
                @msg = flash[:warning2] = "No se encontraron resultados para la búsqueda"
           
            render :empty

        end
end

    def mapa
       @search = params[:title]
       @option = params[:opcion]



       if @option == nil
        @restaurants = Restaurant.all.page(params[:page]).per_page(2)
       else
       @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: (params[:place_id])))))
       end

       if @option == "opcion_uno"
        @restaurants = Restaurant.search(@search).opcion_uno.page(params[:page]).per_page(2)
      
      elsif @option == "opcion_dos"
        @restaurants = Restaurant.search(@search).opcion_dos.page(params[:page]).per_page(2)
      
      elsif @option == "opcion_tres"
         @restaurants = Restaurant.search(@search).opcion_tres.page(params[:page]).per_page(2)
      
      elsif @option == "opcion_cuatro"
         @restaurants = Restaurant.search(@search).opcion_cuatro.page(params[:page]).per_page(2)
      
      elsif @option == "opcion_cinco"
        @restaurants = Restaurant.search(@search).opcion_cinco.page(params[:page]).per_page(2)
      
      elsif @option == "opcion_seis"
        @restaurants = Restaurant.search(@search).opcion_seis.page(params[:page]).per_page(2)
      
      elsif @option == "opcion_siete"
        @restaurants = Restaurant.search(@search).opcion_siete.page(params[:page]).per_page(2)
      
      elsif @option == "opcion_nueve"
        @restaurants = Restaurant.search(@search).opcion_nueve.page(params[:page]).per_page(2)
      
      elsif @option == "opcion_diez"
        @restaurants = Restaurant.search(@search).opcion_diez.page(params[:page]).per_page(2)
      
      elsif @option == "opcion_once"
        @restaurants = Restaurant.search(@search).opcion_once.page(params[:page]).per_page(2)

      elsif @option == "opcion_doce"
        @restaurants = Restaurant.search(@search).opcion_doce.page(params[:page]).per_page(2)
      
      end

      if @restaurants.empty?
        flash.now[:warning] = "No hay restaurantes en este estado"
      end
      # @@restaurants_mapa = @restaurants.pluck('id')
      # redirect_to restaurants_buscador_path(option_busq:'mapa')
  end

    def sort_column
      params[:sort] || "name"
    end

    def sort_direction
      params[:direction] || "asc"
    end

    def avanzada
        @search = params[:title]
        @promo = params[:promo]
        @capacity = params[:capacity]
        @kitchen = KitchenType.all
        @date = params[:date]

        if @promo == '1' 
            @restaurants = Restaurant.discount(params[:kitchen].to_i, params[:capacity].to_i, params[:date][:year], params[:date][:month], params[:date][:day], params[:date][:hour], params[:date][:minute])
        elsif @promo == '2'
            @restaurants = Restaurant.twone(params[:kitchen].to_i, params[:capacity].to_i, params[:date][:year], params[:date][:month], params[:date][:day], params[:date][:hour], params[:date][:minute])
        elsif @promo == '3'
            @restaurants = Restaurant.discount_plate(params[:kitchen].to_i, params[:capacity].to_i, params[:date][:year], params[:date][:month], params[:date][:day], params[:date][:hour], params[:date][:minute])
        else
            @restaurants = Restaurant.all
        end

        if @restaurants.count > 0
            @@restaurants_avanzada = @restaurants.pluck('id')
        redirect_to restaurants_buscador_path(option_busq:'avanzada')
             
            
        else
         render :empty2
          @msg = flash[:warning2] = "No se encontraron resultados para la búsqueda"

         end
         
    end

    #def buscar_x_mas_reservado
    ##Para la muestra de los restaurantes con mas reservas
    ##Primero cuenta la cantidad de restaurant_id repetidos en la tabla reservation y los guarda como total_reservas,
    ##y los compara con el restaurant.id de la tabla restaurant
    ##finalmente los agrupa por restaurant.id y los ordena descendentemente por medio del total_reservas
    ##@restaurants = Restaurant.all #esta linea se uso para probar la conexion entre controlador y base de datos
    #@restaurants = Restaurant.select("restaurants.*, COUNT(reservations.restaurant_id) total_reservas").joins(:reservations).group("restaurants.id").order("total_reservas DESC").page(params[:page]).per_page(8)
    #if @restaurants.empty?
    #flash.now[:warning] = "No hay reservaciones registradas en ningun restaurant"
    #end
    #end
    
  









    def buscar_x_tipo_promo 
        #Para la muestra de restaurantes por su tipo de promocion asociado
        #recibe un texto para el tipo de promocion
        #luego revisa las asociaciones de restaurantes con los tipos de promocion guardados
        #para devolver una lista de restaurantes cuyo tipo de promocion es el indicado
        @promos = params[:promo_type]
        #@restaurants =  Restaurant.find_by_sql("select r.id, r.name, r.address,r.description, p.promo_type from  restaurants r, promos p  where r.id = p.restaurant_id  and  p.promo_type='#{@promos}' ")
        @restaurants = Restaurant.tipo_promo(@promos)
        #valida que haya tipos de promocion en restaurates
        @@restaurants_promo = @restaurants.pluck('id')
        redirect_to restaurants_buscador_path(option_busq:'tipo_promo')
        if @restaurants.empty?
            flash.now[:warning] = "No hay restaurantes registrados con ese tipo de promocion"
        end
    end

    def calification_filter
        @type = params.permit(:type)[:type] #Tipo de calificación
        @value = params.permit(:value)[:value].to_i #Calificación de ambiente
        @state = params.permit(:state)[:state].to_i #Estado

        # @kitchen = params.permit(:kitchen)[:kitchen].to_i #Calificación de cocina
        # @service = params.permit(:service)[:service].to_i #Calificación de servicio
        # @ambience = params.permit(:ambience)[:ambience].to_i #Calificación de ambiente
        @error = true

        if @value.between?(1, 5) && @state.between?(2, 26)

        if  @type == 'kitchen'
            @restaurant_list = Comment.find_by_sql("select r.id as Restaurant, cast(avg(c.kitchen_rating) as decimal (10,1)) as cocina, count(c.id) as total_votos, r.name, r.address, r.description from restaurants r,diners p, comments c,reservations res where res.restaurant_id=r.id and res.diner_id=p.id and c.reservation_id=res.id group by Restaurant having #{@value} BETWEEN (floor(avg(c.kitchen_rating))) AND (floor(avg(c.kitchen_rating))) AND #{@state} IN (select place_id from places where id in (select place_id from places where type_place = 'parroquia' and id = r.place_id)) order by cocina desc")
            @type_search = 'Cocina'
            # @value = @kitchen
            @error = false
        elsif @type == 'service'
            @restaurant_list = Comment.find_by_sql("select r.id as Restaurant, cast(avg(c.service_rating) as decimal (10,1)) as servicio, count(c.id) as total_votos, r.name, r.address, r.description from restaurants r,diners p, comments c,reservations res where res.restaurant_id=r.id and res.diner_id=p.id and c.reservation_id=res.id group by Restaurant having #{@value} BETWEEN (floor(avg(c.service_rating))) AND (floor(avg(c.service_rating))) AND #{@state} IN (select place_id from places where id in (select place_id from places where type_place = 'parroquia' and id = r.place_id)) order by servicio desc")
            @type_search = 'Servicio'
            # @value = @service
            @error = false
        elsif @type == 'ambience'
            @restaurant_list = Comment.find_by_sql("select r.id as Restaurant, cast(avg(c.ambient_rating) as decimal (10,1)) as ambiente, count(c.id) as total_votos, r.name, r.address, r.description from restaurants r,diners p, comments c,reservations res where res.restaurant_id=r.id and res.diner_id=p.id and c.reservation_id=res.id group by Restaurant having #{@value} BETWEEN (floor(avg(c.ambient_rating))) AND (floor(avg(c.ambient_rating))) AND #{@state} IN (select place_id from places where id in (select place_id from places where type_place = 'parroquia' and id = r.place_id)) order by ambiente desc")
            @type_search = 'Ambiente'
            # @value = @ambience
            @error = false
        end
    end
    @estado = Place.find_by_sql("select id, name from places where type_place = 'estado' AND id = #{@state}")
        
        if (!@value.between?(1,5)) && (@value != 0) || @error
          flash.now[:error_ex]= 'No se encontraron resultados para la búsqueda'
        elsif @restaurant_list.empty?
            flash.now[:error_ex]= 'No se encontraron resultados para la búsqueda'
        end   

        if !@state.between?(2,26)
            flash.now[:error_state] = 'El estado ingresado es inválido'
        end

        if @state.between?(2,26)
            flash.now[:correct_state] = 'El estado ingresado es válido'
        end

        if @type != 'ambience' || @type != 'service' || @type != 'kitchen'
            flash.now[:error_type] = 'El tipo de búsqueda no es válida'
        end

        if @type == 'ambience' || @type == 'service' || @type == 'kitchen'
            flash.now[:correct_type] = 'El tipo de búsqueda es válida'
        end

        

        if !@restaurant_list.nil? 
            if @restaurant_list.count > 0
                @@restaurants_calif = @restaurant_list.pluck('restaurant')
                redirect_to restaurants_buscador_path(option_busq:'calif')   
            else 
                render :empty3
            end
        end
    end

    # def buscador
    #     @restaurant = Restaurant.new
        
    #     render partial: 'restaurants/buscador'
    # end


    # def promos_date_filter
    #     @ini = params[:ini]
    #     @fin = params[:fin]

    #     error = false 

    #     if @ini == nil || @ini.empty?
    #         flash.now[:ini_error] = 'Debe establecer la fecha de inicio.'
    #         error = true
    #     end

    #     if @fin == nil || @fin.empty?
    #         flash.now[:fin_error] = 'Debe establecer la fecha de fin.'
    #         error = true
    #     end

    #     if @fin < @ini
    #         flash.now[:error] = 'La fecha de inicio NO puede ser mayor que la fecha de fin.'
    #         error = true
    #     end
        

    #     @restaurants = []
        
    #     if !error
    #         Restaurant.all.each do |r|
    #             if Promo.promo_activa(r.id.to_s, @ini, @fin).length > 0
    #                 @restaurants = @restaurants + [r]
    #             end
    #         end
    #     end 

    # end
    
    def yummys_filter

        @restaurants = []
        # Restaurant.find_by_sql("SELECT r.* FROM restaurants r, places l1 WHERE r.place_id = l1.id AND l1.place_id = #{params[:estado_id]}").each do |r|
        Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: (params[:estado_id]))))).order(:name).group("restaurants.id").each do |r|    
            r.promos.each do |p|
                if p.yumis
                    @restaurants = @restaurants + [r]
                    break
                end
            end  
        end
    @@restaurants_yumis = @restaurants.pluck('id')
    redirect_to restaurants_buscador_path(option_busq:'yumis')
    end

end
