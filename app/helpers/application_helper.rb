module ApplicationHelper
	def sortable(column,title = nil,opcion = nil,menor=nil,mayor=nil)	
		title ||= column.titleize
		if opcion == nil 	
		direction = column == sort_column && sort_direction == "asc NULLS FIRST" ? "desc NULLS LAST" : "asc NULLS FIRST"
		end
		css_class = column == sort_column ? "current #{sort_direction}" : nil
		 link_to title, request.query_parameters.merge({ sort: column, direction: direction, page: nil, opcion: opcion,menor: menor,mayor: mayor})
	end

	def sortable2(title = nil,opcion = nil,menor=nil,mayor=nil)	
		 link_to title, request.query_parameters.merge({ page: nil, opcion: opcion,menor: menor,mayor: mayor})
	end



	def check_for_diner_user
		user = current_user
		if (!user || !user.diner ||  !user.role || user.role.name != 'Comensal')
			render :file => "public/401.html", :status => :unauthorized, :layout => false
		end
	end

	def check_for_restaurant_user
		user = current_user
	
		if (!user || !user.restaurant || !user.role || user.role.name != 'Restaurante')
			render :file => "public/401.html", :status => :unauthorized, :layout => false
		end
	end

	def link_to_add_fields(name, f, association, cssClass, title)  
		new_object = f.object.class.reflect_on_association(association).klass.new  
		fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|  
		  render(association.to_s.singularize + "_fields", :f => builder)  
		end  
		link_to name, "#", :onclick => h("add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")"), :class => cssClass, :title => title, remote: true
	end  
	
end
