class CommentMailer < ApplicationMailer
    def new_recommendation_mail
        @diner = params[:diner]
        @mail = params[:correo]
        @promo = params[:promo]
        @comment = params[:comment]
        @promedio = ((@comment.kitchen_rating + @comment.ambient_rating + @comment.service_rating)/3).round(1)
        @from = 'lapromoucab2020@gmail.com'
        @url = ("http://la-promo.herokuapp.com/restaurant_info/" + @comment.reservation.restaurant_id.to_s)

        mail(from: @from, to: @mail, subject: 'Tu amigo ' + @diner.name + ' te invita a aprovechar esta promocion')
    end
end
