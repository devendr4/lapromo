class CommentRestaurantInviteMailer < ApplicationMailer
    def send_invitation
        @reservation = params[:reservation]
        @diner = @reservation.diner
        @restaurant = @reservation.restaurant

        mail(to: @diner.user.email, subject: "Te invitamos a calificar tu experiencia en #{@restaurant.name}")
    end
end
