class InviteFriendToRestaurantMailer < ApplicationMailer
    def send_invitation
        @reservation = params[:reservation]
        @diner = @reservation.diner
        @restaurant = @reservation.restaurant
        @promo = @reservation.promo

        mail(to: params[:friend_email], subject: "#{@diner.full_name} te ha invitado a conocer el restaurante #{@restaurant.name}")
    end
end
