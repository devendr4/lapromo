class InviteFriendsMailer < ApplicationMailer
    
    def invite_friend_mail
        
        @invitation = params[:invitation]
        @reservation= params[:dinerID]
        mail(to:@invitation.email , subject: "Has sido invitado al:" + @reservation.nombre)

   
    end 
end
