class RestaurantReservationMailer < ApplicationMailer
  def restaurant_cancel_reservation
    @diner = params[:diner]
    @reservation = params[:reservation]
    @mensaje = params[:mensaje]
    mail(to: "#{@diner.user.email}", subject: "Reserva en #{@reservation.restaurant.name} cancelada")
  end
end
