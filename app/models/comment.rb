class Comment < ApplicationRecord
  belongs_to :reservation
  has_many :comment_photos, dependent: :destroy
  has_many :photos, :class_name => :CommentPhoto

  attr_accessor :attached_photos
  attr_accessor :invite_email

  # validations
  validates :kitchen_rating, 
              presence: { message: 'El puntaje de cocina es requerido' },
              numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 5, message: 'El puntaje de cocina debe ser un numero entre 1 y 5' }

  validates :service_rating, 
              presence: { message: 'El puntaje de servicio es requerido' },
              numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 5, message: 'El puntaje de servicio debe ser un numero entre 1 y 5' }

  validates :ambient_rating, 
              presence: { message: 'El puntaje de ambiente es requerido' },
              numericality: { greater_than_or_equal_to: 1, less_than_or_equal_to: 5, message: 'El puntaje de ambiente debe ser un numero entre 1 y 5' }

  validates :description, 
              presence: { message: 'El comentario es requerido' }

  validate :validate_attached_photos, :validate_invite_email


  scope :kitchen_score, ->(id) {joins("JOIN reservations ON comments.reservation_id = reservations.id AND reservations.restaurant_id = " + id).distinct('comments.id').select('comments.*').average("kitchen_rating")}
  scope :ambient_score, ->(id) {joins("JOIN reservations ON comments.reservation_id = reservations.id AND reservations.restaurant_id = " + id).distinct('comments.id').select('comments.*').average("ambient_rating")}
  scope :service_score, ->(id) {joins("JOIN reservations ON comments.reservation_id = reservations.id AND reservations.restaurant_id = " + id).distinct('comments.id').select('comments.*').average("service_rating")}
  scope :comments_of_restaurant, ->(id) {joins("JOIN reservations ON comments.reservation_id = reservations.id AND reservations.restaurant_id = " + id).distinct('comments.id').select('comments.*')}
  # scopes grupo 5
  scope :avg_kitchen_score_rest, -> (id) { where(reservation_id: Reservation.where(restaurant_id: Restaurant.where(id: id).pluck('id')).pluck('id')).average('kitchen_rating') }
  scope :avg_ambient_score_rest, -> (id) { where(reservation_id: Reservation.where(restaurant_id: Restaurant.where(id: id).pluck('id')).pluck('id')).average('ambient_rating') }
  scope :avg_service_score_rest, -> (id) { where(reservation_id: Reservation.where(restaurant_id: Restaurant.where(id: id).pluck('id')).pluck('id')).average('service_rating') }
  scope :number_comments_rest, -> (id) { where(reservation_id: Reservation.where(restaurant_id: Restaurant.where(id: id).pluck('id')).pluck('id')).count('id') }

  private 
    def validate_attached_photos
      if attached_photos && attached_photos.length > 0
        attached_photos.each do |photo|
          if !photo.content_type.in?(%('image/jpeg image/png'))
            errors.add(:photos, "Alguna de las imagenes adjuntadas no esta en los formatos soportados (jpg, png)")
            break
          elsif photo.size > 2.megabytes
            errors.add(:photos, "Alguna de las imagenes adjuntadas excede los 2 megabytes")
          end
        end     
      end
    end

    def validate_invite_email
        if invite_email && ! invite_email.empty?
          unless invite_email =~ URI::MailTo::EMAIL_REGEXP
            errors.add(:invite_email, "El email para invitacion debe ser valido")
          end
        end
    end

    after_create do
      if attached_photos && attached_photos.length > 0
        attached_photos.each do |photo|
          path = Services::ImageUploadService.upload(photo.tempfile.path)

          self.photos.create(path: path).save
        end     
      end
    end
end
