module PermisosConcern
    extend ActiveSupport::Concern
    #Comprobar si es rol administrador
    def is_admin?
        self.role_id == 1 || self.role.name == "Administrador"
    end
    #comprobar si es rol restaurante
    def is_restaurant?
        self.role_id == 2 || self.role.name == "Restaurante"
    end
    #comprobar si es rol comensal
    def is_diner?
        self.role_id == 3 || self.role.name == "Comensal"
    end
    
end