class MenuType < ApplicationRecord
  belongs_to :restaurant
  has_many :plates, :dependent => :delete_all
  accepts_nested_attributes_for :plates
  validates :name, :presence => {:message => "Falta el nombre"}
  
  def self.search(term, page)
		if term
		  where('nombre LIKE ?', "%#{term}%").paginate(page: page, per_page: 5).order('id DESC')
		else
		  paginate(page: page, per_page: 5).order('id DESC') 
		end
	end

end
