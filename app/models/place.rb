class Place < ApplicationRecord
  self.inheritance_column = nil
  belongs_to :place, optional:true
  has_one :photo
  has_many :places
  has_many :diners
  has_many :restaurants

scope :list_of_places, ->{find_by_sql('
    select distinct estado.*,photo from places
    inner join restaurants on restaurants.place_id = places.id
    inner join places as municipio on places.place_id = municipio.id
    inner join places as estado on municipio.place_id = estado.id
    inner join photos  on photos.place_id = estado.id')}

  #scope :list_of_places ,-> {joins(:photo).where(type_place: 'estado').select("places.* ,photos.photo").distinct}



 scope :restaurantes, ->{
   where(id: Place.where(id: Place.where(id:Restaurant.pluck('place_id')).pluck('place_id')).pluck('place_id'))
  }

  scope :estado, ->(placeid){
  	where(id: Place.where(id: Place.where(id: Place.where(id: placeid)).pluck('place_id')).pluck('place_id'))
  }
  scope :municipio, ->(placeid){
	where(id: Place.where(id: Place.where(id: placeid)).pluck('place_id'))
   }



end
