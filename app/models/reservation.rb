class Reservation < ApplicationRecord
  belongs_to :promo
  belongs_to :restaurant
  belongs_to :hist_yumi, required: false
  belongs_to :diner
  has_one :comment

  #Para obtener todas las reservaciones
  #Obtiene los datos de las reservas, el nombre y la direccion del restaurant y los puntos yumi 
  #compara el id del restaurant con el restaurant_id de la reserva, el hist_yumi_id de la reserva con el id de hist_yumis
  #finalmente compara el diner_id de la reserva con el dinerid que se le esta pasando
  scope :todas_reservaciones, ->(dinerid) {
    find_by_sql("select resv.id, resv.reservation_date, resv.people_num, resv.status, resv.restaurant_id, resv.promo_id, r.name as rest_name, r.address as rest_address, hy.quantity 
    from  reservations resv,  restaurants r, hist_yumis hy 
    where (r.id = resv.restaurant_id and resv.hist_yumi_id = hy.id and resv.diner_id='#{dinerid}') ")
  }

  #para obtener todas las reservaciones dentro de un restaurant
  #obtiene el nombre del usuario y los datos de las reservas
  #compara el id del rest_id de la reserva con el del id del restaurant, el id del user con
  #el user_id de la reserva
  scope :todas_rest, ->(id) {
    find_by_sql("select resv.id, resv.reservation_date, resv.people_num, resv.status,
      resv.restaurant_id, resv.promo_id, resv.diner_id, d.name as comensal_name, 
      d.last_name as comensal_apellido, d.gender as comensal_genero,
      extract(year from age(d.birth_date)) as comensal_nac, 
      d.user_id, d.identity_card as identificacion,
      p.description as desc, hy.quantity as yum
      from reservations resv, restaurants r, diners d, promos p, hist_yumis hy
      where (r.id = resv.restaurant_id and resv.diner_id = d.id and resv.promo_id = p.id and resv.hist_yumi_id = hy.id and r.user_id='#{id}')
      order by (extract(year from resv.reservation_date), extract(month from resv.reservation_date), extract(day from resv.reservation_date), extract(hour from resv.reservation_date), extract(minute from resv.reservation_date)) ")
  }

  #para obtener todas las reservaciones dentro de un restaurant dentro de una fecha especifica
  #obtiene el nombre del usuario y los datos de las reservas
  #compara el id del rest_id de la reserva con el del id del restaurant, el id del user con
  #el user_id de la reserva y la fecha de la reserva
  scope :todas_rest_f, ->(id,date) {
    find_by_sql("select resv.id,resv.reservation_date, resv.people_num, resv.status,
      resv.restaurant_id, resv.promo_id, resv.diner_id, d.name as comensal_name, 
      d.last_name as comensal_apellido, d.gender as comensal_genero,
      extract(year from age(d.birth_date)) as comensal_nac, 
      d.user_id, d.identity_card as identificacion,
      p.description as desc, hy.quantity as yum 
      from reservations resv, restaurants r, diners d, promos p, hist_yumis hy
      where (r.id = resv.restaurant_id and resv.diner_id = d.id and resv.promo_id = p.id and resv.hist_yumi_id = hy.id and r.user_id='#{id}' and 
        (extract(day from resv.reservation_date) = extract(day from timestamp '#{date}'))and 
        (extract(month from resv.reservation_date) = extract(month from timestamp '#{date}'))and
        (extract(year from resv.reservation_date) = extract(year from timestamp '#{date}')))
      order by (extract(year from resv.reservation_date), extract(month from resv.reservation_date), extract(day from resv.reservation_date), extract(hour from resv.reservation_date), extract(minute from resv.reservation_date)) ")
  }

  # Grupo 7
  scope :no_comment, ->(id) {includes(:comment).where("comments.id IS null").where("reservations.diner_id = " + id).where("UPPER(reservations.status) like UPPER('Efectuada')").references(:comment).select("reservations.*")}
  # Fin grupo 7

  CANCELED = 'Cancelada'
  USED = 'Efectuada'
  NOT_USED = 'No efectuada'
  PENDING = 'Pendiente'

  def canceled?
    self.status && self.status.downcase == CANCELED.downcase
  end

  def used?
    self.status && self.status.downcase == USED.downcase
  end

  def not_used?
    self.status && self.status.delete(' ').downcase == NOT_USED.delete(' ').downcase
  end
  
  def pending?
    self.status && self.status.downcase == PENDING.downcase
  end

  validates :status, inclusion: { in: [CANCELED.downcase, USED.downcase, NOT_USED.delete(' ').downcase, PENDING.downcase] }

  before_validation do
    self.status = self.status.delete(' ').downcase
  end

  before_save do
    if self.canceled?
      self.status = CANCELED
    elsif self.used?
      self.status = USED
    elsif self.not_used?
      self.status = NOT_USED
    else
      self.status = PENDING
    end
  end

  scope :actual, lambda {
    where('reservation_date >= ? and status = ? ', DateTime.now, 'Pendiente')
  }
  scope :restaurante, ->(id) { where(restaurant_id: id) }
  scope :hora, ->(hora) { where('reservation_date::time = ? ', hora) }
  scope :fecha, ->(fecha) { where('DATE(reservation_date) = ?', fecha) }
  scope :rango_fecha, lambda { |fecha_ini, fecha_fin|
    where('DATE(reservation_date) BETWEEN ? AND ? ', fecha_ini, fecha_fin)
  }

end
