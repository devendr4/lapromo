class Restaurant < ApplicationRecord
  belongs_to :user
  belongs_to :place
  has_many :promos
  has_many :menu_types
  has_many :payments
  has_many :favorites
  has_many :diners, through: :favorites
  has_many :reservations
  has_many :comments, through: :reservations
  has_many :photos, dependent: :destroy
  has_many :restaurant_kitchens
  has_many :kitchen_types, through: :restaurant_kitchens

    scope :restaurant_of_place, ->(id) {find_by_sql('
      select rest.*,ph.*,a.*
         from (select array_to_json(array_agg(row_to_json(t))) as kt, rest_id as id
         from (
           select a.id,
      a.name,
      c.id as rest_id
      from kitchen_types a
           inner join restaurant_kitchens b on  b.kitchen_type_id = a.id
           INNER JOIN restaurants c on b.restaurant_id = c.id
           ) t group by t.rest_id) a


           INNER JOIN restaurants AS rest ON rest.id = a.id
     LEFT JOIN  (select array_to_json(array_agg(row_to_json(g))) as photos_list, rest_id as idph
         from (
           select p.photo,
      c.id as rest_id
      from photos p
      INNER JOIN restaurants c ON p.restaurant_id = c.id
           ) g group by g.rest_id) ph  ON rest.id = ph.idph

           INNER JOIN places AS parroquia ON parroquia.id = rest.place_id
           INNER JOIN places AS municipio ON municipio.id = parroquia.place_id
     INNER JOIN places AS estado ON estado.id = municipio.place_id
     WHERE estado.id ='+id)

                                        }


    scope :restaurant_of_list, ->(id,query) {find_by_sql('
                   select*
                    from (select array_to_json(array_agg(row_to_json(t))) as kt, rest_id as id
                    from (
                      select a.id,
                 a.name,
                 c.id as rest_id
                 from kitchen_types a
                      inner join restaurant_kitchens b on  b.kitchen_type_id = a.id
                      INNER JOIN restaurants c on b.restaurant_id = c.id
                      ) t group by t.rest_id) a


                      INNER JOIN restaurants AS rest ON rest.id = a.id
                INNER JOIN  (select array_to_json(array_agg(row_to_json(g))) as photos, rest_id as idph
                    from (
                      select p.photo,
                 c.id as rest_id
                 from photos p
                 INNER JOIN restaurants c on p.restaurant_id = c.id
                      ) g group by g.rest_id) ph  ON rest.id = ph.idph

                            where 1 = 1 ' +query)

                                             }


    scope :restaurant_filter_name, ->(q) {find_by_sql('
                 select*
                  from (select array_to_json(array_agg(row_to_json(t))) as kt, rest_id as id
                  from (
                    select a.id,
              a.name,
              c.id as rest_id
              from kitchen_types a
                    inner join restaurant_kitchens b on  b.kitchen_type_id = a.id
                    INNER JOIN restaurants c on b.restaurant_id = c.id
                    ) t group by t.rest_id) a


                    INNER JOIN restaurants AS rest ON rest.id = a.id
             INNER JOIN  (select array_to_json(array_agg(row_to_json(g))) as photos, rest_id as idph
                  from (
                    select p.photo,
              c.id as rest_id
              from photos p
              INNER JOIN restaurants c on p.restaurant_id = c.id
                    ) g group by g.rest_id) ph  ON rest.id = ph.idph

                          where rest.name LIKE '+' \'%'+ q +'%\'')

                                        }

    scope :restaurant_filter_address, ->(q) {find_by_sql('
           select*
            from (select array_to_json(array_agg(row_to_json(t))) as kt, rest_id as id
            from (
              select a.id,
        a.name,
        c.id as rest_id
        from kitchen_types a
              inner join restaurant_kitchens b on  b.kitchen_type_id = a.id
              INNER JOIN restaurants c on b.restaurant_id = c.id
              ) t group by t.rest_id) a


              INNER JOIN restaurants AS rest ON rest.id = a.id
       INNER JOIN  (select array_to_json(array_agg(row_to_json(g))) as photos, rest_id as idph
            from (
              select p.photo,
        c.id as rest_id
        from photos p
        INNER JOIN restaurants c on p.restaurant_id = c.id
              ) g group by g.rest_id) ph  ON rest.id = ph.idph

                    where rest.address LIKE '+' \'%'+ q +'%\'')

                                             }


    scope :restaurant_filter_kitchentype, ->(q) {find_by_sql('
           select*
            from (select array_to_json(array_agg(row_to_json(t))) as kt, rest_id as id
            from (
              select a.id,
        a.name,
        c.id as rest_id
        from kitchen_types a
              inner join restaurant_kitchens b on  b.kitchen_type_id = a.id
              AND b.kitchen_type_id ='+q+'
              INNER JOIN restaurants c on b.restaurant_id = c.id
              ) t group by t.rest_id) a


              INNER JOIN restaurants AS rest ON rest.id = a.id
       INNER JOIN  (select array_to_json(array_agg(row_to_json(g))) as photos, rest_id as idph
            from (
              select p.photo,
        c.id as rest_id
        from photos p
        INNER JOIN restaurants c on p.restaurant_id = c.id
              ) g group by g.rest_id) ph  ON rest.id = ph.idph

                  ')

                                                 }

    scope :filter_generic, ->(query) {find_by_sql('

      SELECT distinct restaurants.id as id FROM restaurants
      LEFT JOIN promos ON promos.restaurant_id = restaurants.id
      LEFT JOIN restaurant_kitchens ON restaurant_kitchens.restaurant_id = restaurants.id
      LEFT JOIN promo_calendars ON promo_calendars.promo_id = promos.id
      LEFT JOIN calendars ON calendars.id = promo_calendars.calendar_id
      WHERE 1 = 1

      '+query)
                                    }


    #scope :restaurant_of_place, ->(id) {joins(:restaurant_kitchens).joins(:kitchen_types).where(place_id: id).select('*').distinct}

    validates :name,:presence => {:message => "El campo nombre no puede estar vacio"},
        format: {with: /\A[a-zA-Z\s]+\z/, message: "El campo nombre no puede tener caracteres especiales"}

    # VALIDACIONES PARA EL CAMPO DIRECCION DEL RESTAURANTE
    validates :address,:presence => {:message => "El campo dirección no puede estar vacio"}

    ## VALIDACIONES PARA EL CAMPO ZONA POSTAL DEL RESTAURANTE
    validates :zip_code,:presence => {:message => "El campo zona postal no puede estar vacio"},
        numericality: {only_integer: true, message: "Debe ingresar solo números"},
        length: { is: 4, message: "La zona postal tiene un tamaño de solo 4 números"}

    ## VALIDACIONES PARA EL CAMPO CAPACIDAD DEL RESTAURANTE
    validates :capacity,:presence => {:message => "El campo capacidad no puede estar vacio"},
        numericality: {only_integer: true, message: "Debe ingresar solo números"}
    # numericality: { greater_than_or_equal_to: 10, message: "La capacidad debe ser mínimo para 10 personas"}

    ## VALIDACIONES PARA EL CAMPO DESCRIPCION DEL RESTAURANTE
    validates :description,:presence => {:message => "El campo descripción no puede estar vacio"}

    ## VALIDACIONES PARA EL CAMPO RIF DEL RESTAURANTE
    validates :rif,:presence => {:message => "El campo RIF no puede estar vacio"},
        uniqueness: {case_sensitive: true, message: "Ya existe este RIF en nuestros registros"},
        format: {with: /\AJ-([0-9]{8})-([0-9]{1}\Z)/, message: "Formato del RIF incorrecto"},
            length: { is: 12, message: "El RIF tiene un tamaño de 12 caracteres"}

  accepts_nested_attributes_for :photos, reject_if: :all_blank, allow_destroy: true
  accepts_nested_attributes_for :user




  ## SCOPE PARA RECIBIR CONSULTA DE USUARIOS NO APROBADOS
  scope :no_aprobados, -> { Restaurant.joins(:user).where("users.approved = false") }
  ## SCOPE PARA RECIBIR CONSULTA DE USUARIOS APROBADOS
  scope :aprobados, -> { Restaurant.joins(:user).where("users.approved = true") }
  # Ex:- scope :active, -> {where(:active => true)}

  ## VALIDACIONES PARA EL CAMPO HORARIO INICIO DEL RESTAURANTE
  validates :start_time,:presence => {:message => "Debe indicar el horario de inicio del restaurant"}

  ## VALIDACIONES PARA EL CAMPO HORARIO FIN DEL RESTAURANTE
  validates :end_time,:presence => {:message => "Debe indicar el horario de cierre del restaurant"}

  validates_numericality_of :place_id, greater_than: 361, less_than: 1500
  validate :times?

  def times?

    if !end_time.blank? and !start_time.blank?
      if end_time <= start_time
        errors.add(:end_time, 'El campo Hora de cierre no puede ser menor o igual que el campo hora de apertura')
      end
    end

  end



  scope :places, -> (placeid){
   (where place_id: Place.where(place_id: Place.where(place_id: Place.where(id: placeid).pluck('id')).pluck('id')).pluck('id')).count('place_id')
  }

################Grupo 5##############

  #Para buscar restaurantes mas reservados
  #Primero cuenta la cantidad de restaurant_id repetidos en la tabla reservation y los guarda como total_reservas,
  #y los compara con el restaurant.id de la tabla restaurant
  #finalmente los agrupa por restaurant.id y los ordena descendentemente por medio del total_reservas
  scope :mas_reservados, -> {
    select("restaurants.*, COUNT(reservations.restaurant_id) total_reservas").joins(:reservations).group("restaurants.id").order("total_reservas DESC")
  }

  #Para Buscar restaurantes por tipo de promocion
  #Obtiene datos de los restaurantes y de la promo, comparando el id de restaurante con el id_restaurante de la promo
  #Tambien compara el tipo de promo con el promo_type que se le esta pasando
  scope :tipo_promo, -> (promo_type){
    find_by_sql("select r.id, r.name, r.address,r.description, p.promo_type, p.description as detalle from  restaurants r, promos p  where r.id = p.restaurant_id  and  p.promo_type='#{promo_type}'")
  }
  scope :for_order, ->(ids) {
    order = sanitize_sql_array(
      ["position((',' || id::text || ',') in ?)", ids.join(',') + ',']
    )
    where(:id => ids).order(Arel.sql(order))
  }

    scope :menus, ->(type){
        a = Restaurant.where(id: MenuType.where(name: type).pluck('restaurant_id')).pluck('id')
    }

  def self.search(search)
    if search
      where('name ILIKE ?',"%#{search}%")
    else
      all
    end
  end


   def self.plates_desc
    a = Restaurant.find_by_sql("SELECT AVG(plates.price) AS average_price, restaurants.id FROM plates INNER JOIN menu_types ON menu_types.id = plates.menu_type_id  INNER JOIN restaurants ON restaurant_id=restaurants.id GROUP BY restaurants.id ORDER BY average_price DESC ").pluck('id')
   @result = for_order(a)
   end

    def self.plates_asc
      a = Restaurant.find_by_sql("SELECT AVG(plates.price) AS average_price, restaurants.id FROM plates INNER JOIN menu_types ON menu_types.id = plates.menu_type_id  INNER JOIN restaurants ON restaurant_id=restaurants.id GROUP BY restaurants.id ORDER BY average_price").pluck('id')
     @result = for_order(a)
   end

   def self.rating_desc
    a = Restaurant.find_by_sql("SELECT AVG(comments.service_rating + comments.kitchen_rating + comments.ambient_rating)/3 AS average, restaurants.id FROM comments INNER JOIN reservations ON reservations.id = comments.reservation_id INNER JOIN restaurants ON restaurants.id = reservations.restaurant_id GROUP BY restaurants.id ORDER BY average DESC").pluck('id')
    @result = for_order(a)
   end

   def self.rating_asc
    a = Restaurant.find_by_sql("SELECT AVG(comments.service_rating + comments.kitchen_rating + comments.ambient_rating)/3 AS average, restaurants.id FROM comments INNER JOIN reservations ON reservations.id = comments.reservation_id INNER JOIN restaurants ON restaurants.id = reservations.restaurant_id GROUP BY restaurants.id ORDER BY average ASC").pluck('id')
    @result = for_order(a)
   end

   def self.promo_desc
     a = Promo.joins(:restaurant).select('promo_type','restaurant_id').order('promo_type DESC').pluck('id')
     @result = for_order(a)
   end

   def self.opcion_uno
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([3])))))
    @result = for_order(a)
   end

   def self.opcion_dos
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([6])))))
    @result = for_order(a)
   end

   def self.opcion_tres
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([25])))))
    @result = for_order(a)
   end

   def self.opcion_cuatro
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([11])))))
    @result = for_order(a)
   end

   def self.opcion_cinco
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([14])))))
    @result = for_order(a)
   end

   def self.opcion_seis
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([15])))))
    @result = for_order(a)
   end

   def self.opcion_siete
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([17])))))
    @result = for_order(a)
   end

   def self.opcion_nueve
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([20])))))
    @result = for_order(a)
   end

   def self.opcion_diez
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([21])))))
    @result = for_order(a)
   end

   def self.opcion_once
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([23])))))
    @result = for_order(a)
   end

   def self.opcion_doce
    a = @restaurants = Restaurant.where(place_id: Place.where(place_id: Place.where(place_id: Place.where(id: ([24])))))
    @result = for_order(a)
   end


  def self.comments_desc(resultado=nil)
    a = Restaurant.find_by_sql("SELECT COUNT(*) AS count_all, restaurants.id FROM comments INNER JOIN reservations ON reservations.id = comments.reservation_id  INNER JOIN restaurants on restaurants.id = reservations.restaurant_id GROUP BY restaurants.id ORDER BY count_all DESC
      ").pluck('id')
    if resultado==nil
      return for_order(a)
    else
      return for_order(a & resultado)
    end
  end

    def self.comments_asc(resultado=nil)
    a = Restaurant.find_by_sql("SELECT COUNT(*) AS count_all, restaurants.id FROM comments INNER JOIN reservations ON reservations.id = comments.reservation_id  INNER JOIN restaurants on restaurants.id = reservations.restaurant_id GROUP BY restaurants.id ORDER BY count_all").pluck('id')
    if resultado==nil
      return for_order(a)
    else
      return for_order(a & resultado)
    end
  end



   def self.plates_desc(resultado=nil)
    a = Restaurant.find_by_sql("SELECT AVG(plates.price) AS average_price, restaurants.id FROM plates INNER JOIN menu_types ON menu_types.id = plates.menu_type_id  INNER JOIN restaurants ON restaurant_id=restaurants.id GROUP BY restaurants.id ORDER BY average_price DESC ").pluck('id')
    if resultado==nil
      return for_order(a)
    else
      return for_order(a & resultado)
    end
   end

    def self.plates_asc(resultado=nil)
      a = Restaurant.find_by_sql("SELECT AVG(plates.price) AS average_price, restaurants.id FROM plates INNER JOIN menu_types ON menu_types.id = plates.menu_type_id  INNER JOIN restaurants ON restaurant_id=restaurants.id GROUP BY restaurants.id ORDER BY average_price").pluck('id')
      if resultado==nil
        return for_order(a)
      else
        return for_order(a & resultado)
      end
   end

   def self.rating_desc(resultado=nil)
    a = Restaurant.find_by_sql("SELECT AVG(comments.service_rating + comments.kitchen_rating + comments.ambient_rating)/3 AS average, restaurants.id FROM comments INNER JOIN reservations ON reservations.id = comments.reservation_id INNER JOIN restaurants ON restaurants.id = reservations.restaurant_id GROUP BY restaurants.id ORDER BY average DESC").pluck('id')
     if resultado==nil
        return for_order(a)
     else
        return for_order(a & resultado)
     end
   end

   def self.rating_asc(resultado=nil)
    a = Restaurant.find_by_sql("SELECT AVG(comments.service_rating + comments.kitchen_rating + comments.ambient_rating)/3 AS average, restaurants.id FROM comments INNER JOIN reservations ON reservations.id = comments.reservation_id INNER JOIN restaurants ON restaurants.id = reservations.restaurant_id GROUP BY restaurants.id ORDER BY average ASC").pluck('id')
      if resultado==nil
          return for_order(a)
      else
         return for_order(a & resultado)
      end
   end

   def self.promo_desc(resultado=nil)
    promos = Promo.where(promo_type:'% de descuento').or(Promo.where(promo_type:'% de descuento por plato')).sort_by{|p| p.description[0..1].to_i}.reverse#.pluck('restaurant_id')
    promos2 = Promo.where(promo_type:'2x1').sort_by(&:description)

    for i in 0..promos.length-1 do
        if promos[i].description[0..1].to_i  < 50
            print i
          for j in 0..promos2.length-1 do
            promos.insert(i,promos2[j])
          end
          break
        end

     end
    promos = promos.pluck('restaurant_id')
    if resultado==nil
      for_order(promos)
    else
      for_order(promos & resultado)
    end
   end

   def self.rango_price(menor=2,mayor=3)
    a = []
    b = ActiveRecord::Base.connection.exec_query("SELECT AVG(plates.price) AS average_price, restaurants.id FROM plates INNER JOIN menu_types ON menu_types.id = plates.menu_type_id  INNER JOIN restaurants ON restaurant_id=restaurants.id GROUP BY restaurants.id").rows
     b.each do |k,v|
        if mayor < 20 then
          if ((k >= menor)and(k <= mayor))then
            a.append(v)
          end

        else

          if k >= menor then
            a.append(v)
          end

        end
      end
   return for_order(a)
  end

  def self.rango_rating(menor=2,mayor=3)
    a = []
    b = ActiveRecord::Base.connection.exec_query("SELECT AVG(comments.service_rating + comments.kitchen_rating + comments.ambient_rating)/3 AS average, restaurants.id FROM comments INNER JOIN reservations ON reservations.id = comments.reservation_id INNER JOIN restaurants ON restaurants.id = reservations.restaurant_id GROUP BY restaurants.id").rows
      b.each do |k,v|
        if mayor == 5 then
          if ((k.to_i >= menor)and(k.to_i <= mayor))then
            a.append(v)
          end
        else
           if ((k.to_i >= menor)and(k.to_i < mayor))then
            a.append(v)
           end
        end
      end
      return for_order(a)
  end

  def self.menu_type(tipos)
    c = []
    b = []
    if tipos
        for i in 0..tipos.length-1 do
          c.append(Restaurant.menus(tipos[i]))
        end
        for i in 0..c.length-1 do
          for j in 0..c[i].length-1 do
            b.append(c[i][j])
          end
        end
        if b.length > 0
        return for_order(b.uniq)
      else all
      end
      else
        all
    end
   end

   def self.urlplatos(platos)
        c=''
     if platos
     for i in 0..platos.length-1 do
        c = c+'&plates_id%5B%5D='+ platos[i]
     end
     return c
    else
    return nil
        end
   end

   def self.discount(kitchen, capacity, year, month, day, hour, minute)

    if month.to_i < 10 && day.to_i < 10
      month = "0" + month.to_s
      day = "0" + day.to_s
    elsif month.to_i < 10
      month = "0" + month.to_s
    elsif day.to_i < 10
      day = "0" + day.to_s
    end

    a = Restaurant.find_by_sql("select distinct r.id from restaurants r, promos p, restaurant_kitchens rk, kitchen_types kt
    where r.id = p.restaurant_id and r.id = rk.restaurant_id and rk.kitchen_type_id = kt.id and p.promo_type = '% de descuento' and kt.id = #{kitchen} and r.capacity = #{capacity}
    and r.id IN (select distinct r.id
    from restaurants r, promos p, promo_calendars pc, calendars c, promo_calendars pc2, calendars c2
    where r.id = p.restaurant_id and p.id = pc.promo_id and pc.calendar_id = c.id and p.id = pc2.promo_id and pc2.calendar_id = c2.id
    and (c.final_date::text LIKE '%#{year.to_s}-#{month.to_s}-#{day.to_s} #{hour.to_s}:#{minute.to_s}%' or c2.final_date::text LIKE '%#{year.to_s}-#{month.to_s}-#{day.to_s} #{hour.to_s}:#{minute.to_s}%'))").pluck('id')
    @result = for_order(a)
  end

  def self.twone(kitchen, capacity, year, month, day, hour, minute)

    if month.to_i < 10 && day.to_i < 10
      month = "0" + month.to_s
      day = "0" + day.to_s
    elsif month.to_i < 10
      month = "0" + month.to_s
    elsif day.to_i < 10
      day = "0" + day.to_s
    end

    a = Restaurant.find_by_sql("select distinct r.id from restaurants r, promos p, restaurant_kitchens rk, kitchen_types kt
    where r.id = p.restaurant_id and r.id = rk.restaurant_id and rk.kitchen_type_id = kt.id and p.promo_type = '2x1' and kt.id = #{kitchen} and r.capacity = #{capacity}
    and r.id IN (select distinct r.id
    from restaurants r, promos p, promo_calendars pc, calendars c, promo_calendars pc2, calendars c2
    where r.id = p.restaurant_id and p.id = pc.promo_id and pc.calendar_id = c.id and p.id = pc2.promo_id and pc2.calendar_id = c2.id
    and (c.final_date::text LIKE '%#{year.to_s}-#{month.to_s}-#{day.to_s} #{hour.to_s}:#{minute.to_s}%' or c2.final_date::text LIKE '%#{year.to_s}-#{month.to_s}-#{day.to_s} #{hour.to_s}:#{minute.to_s}%'))").pluck('id')
    @result = for_order(a)
  end

  def self.discount_plate(kitchen, capacity, year, month, day, hour, minute)

    if month.to_i < 10 && day.to_i < 10
      month = "0" + month.to_s
      day = "0" + day.to_s
    elsif month.to_i < 10
      month = "0" + month.to_s
    elsif day.to_i < 10
      day = "0" + day.to_s
    end

    a = Restaurant.find_by_sql("select distinct r.id from restaurants r, promos p, restaurant_kitchens rk, kitchen_types kt
    where r.id = p.restaurant_id and r.id = rk.restaurant_id and rk.kitchen_type_id = kt.id and p.promo_type = '% de descuento por plato' and kt.id = #{kitchen} and r.capacity = #{capacity}
    and r.id IN (select distinct r.id
    from restaurants r, promos p, promo_calendars pc, calendars c, promo_calendars pc2, calendars c2
    where r.id = p.restaurant_id and p.id = pc.promo_id and pc.calendar_id = c.id and p.id = pc2.promo_id and pc2.calendar_id = c2.id
    and (c.final_date::text LIKE '%#{year.to_s}-#{month.to_s}-#{day.to_s} #{hour.to_s}:#{minute.to_s}%' or c2.final_date::text LIKE '%#{year.to_s}-#{month.to_s}-#{day.to_s} #{hour.to_s}:#{minute.to_s}%'))").pluck('id')
    @result = for_order(a)
  end
end
