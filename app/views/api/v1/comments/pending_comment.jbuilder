json.reservations @reservations do |reservation|
    json.id reservation.id
    json.restaurant reservation.restaurant.name
end
