class AddTotalYumisToDiners < ActiveRecord::Migration[5.2]
  def change
    add_column :diners, :total_yumis, :integer
  end
end
