class ChangeRatingTypesOnCommentsTableToHaveDecimals < ActiveRecord::Migration[5.2]
  def change
    change_column :comments, :kitchen_rating, :decimal, precision: 10, scale: 2
    change_column :comments, :service_rating, :decimal, precision: 10, scale: 2
    change_column :comments, :ambient_rating, :decimal, precision: 10, scale: 2
  end
end
