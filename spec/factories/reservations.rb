FactoryBot.define do
  factory :reservation do
    reservation_date { "2019-11-21 23:37:19" }
    people_num { 2 }
    status { Reservation::PENDING }
    promo
    hist_yumi_id { nil }
    restaurant
    diner
  end
end
