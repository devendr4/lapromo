# Preview all emails at http://localhost:3000/rails/mailers/reservations_mailer
class ReservationsMailerPreview < ActionMailer::Preview
	include FactoryBot::Syntax::Methods
    
    def cancel_reservation_email
        diner = create(:diner)
        reserva = create(:reservation)

        ReservationsMailer.with(diner: diner, reserva: reserva).cancel_reservation_email
    end
end
