require "rails_helper"

RSpec.describe Comensal::HistYumisController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/comensal/hist_yumis").to route_to("comensal/hist_yumis#index")
    end

    it "routes to #new" do
      expect(:get => "/comensal/hist_yumis/new").to route_to("comensal/hist_yumis#new")
    end

    it "routes to #show" do
      expect(:get => "/comensal/hist_yumis/1").to route_to("comensal/hist_yumis#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/comensal/hist_yumis/1/edit").to route_to("comensal/hist_yumis#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/comensal/hist_yumis").to route_to("comensal/hist_yumis#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/comensal/hist_yumis/1").to route_to("comensal/hist_yumis#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/comensal/hist_yumis/1").to route_to("comensal/hist_yumis#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/comensal/hist_yumis/1").to route_to("comensal/hist_yumis#destroy", :id => "1")
    end
  end
end
