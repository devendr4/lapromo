require 'rails_helper'

RSpec.describe "comensal/hist_yumis/edit", type: :view do
  before(:each) do
    @comensal_hist_yumi = assign(:comensal_hist_yumi, HistYumi.create!())
  end

  it "renders the edit comensal_hist_yumi form" do
    render

    assert_select "form[action=?][method=?]", comensal_hist_yumi_path(@comensal_hist_yumi), "post" do
    end
  end
end
