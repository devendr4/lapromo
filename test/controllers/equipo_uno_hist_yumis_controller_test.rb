require 'test_helper'

class EquipoUnoHistYumisControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  setup do
    @controller = Comensal::HistYumisController.new
    Place.create(id:1234,name: "Libertador",type_place:"Municipio")
    @user = User.create(id:111,user_name:"marcolauro955",email: "marcolauro955@gmail.com",password: 123456,
      password_confirmation: 123456, role_id: 3,confirmed_at: Time.now)
    @diner= Diner.create(id:30,name:"Marco",last_name:"lauro",identity_card: 1415555,gender: "M",
      user_id:111,place_id:1234,birth_date:"1995-10-17")
    sign_in @user
  end

  
  test "should_response_motodo_mis_yumis" do
    get :mis_yumis
    assert_response :success
  end

  test "should_response_motodo_yumis_amigos" do
    get :yumis_amigos
    assert_response :success
  end
  test "should_response_motodo_yumis_reservas_efectuadas" do
    get :yumis_reservas_efectuadas
    assert_response :success
  end
  
  test "should_response_motodo_yumis_reservas_noefectuadas" do
    get :yumis_reservas_noefectuadas
    assert_response :success
  end

  test "should_response_motodo_yumis_reservas_canceladas" do
    get :yumis_reservas_canceladas
    assert_response :success
  end
  
  test "should_response_no_hay_yumis_totales" do
    get :mis_yumis
    assert_equal "Usted no tiene puntos yumis", flash[:warning]
  end
  
  test "should_response_si_hay_yumis_totales" do
    HistYumi.create(type_hist_yumis:"Reserva no efectuada", quantity:300, diner_id: @diner.id)
    get :mis_yumis
    assert_equal "Si hay yumis", flash[:success]
  end
  
  test "should_response_no_hay_yumis_por_efectuadas" do
    get :yumis_reservas_efectuadas
    assert_equal "Usted no tiene puntos yumis por reservas efectuadas", flash[:warning]
  end
  
  test "should_response_si_hay_yumis_por_efectuadas" do
    HistYumi.create(type_hist_yumis:"Reserva efectuada", quantity:300, diner_id: @diner.id)
    get :yumis_reservas_efectuadas
    assert_equal "Si hay yumis por reservas efectuadas", flash[:success]
  end
  
  test "should_response_no_hay_yumis_por_invitar_amigos" do
    get :yumis_amigos
    assert_equal "Usted no tiene puntos yumis por invitar amigos", flash[:warning]
  end
  
  test "should_response_si_hay_yumis_por_invitar_amigos" do
    HistYumi.create(type_hist_yumis:"Invitar amigo", quantity:300, diner_id: @diner.id)
    get :yumis_amigos
    assert_equal "Si hay yumis por invitar amigo", flash[:success]
  end
  
  test "should_response_no_hay_yumis_por_no_efectuadas" do
    get :yumis_reservas_noefectuadas
    assert_equal "Usted no tiene puntos yumis por reservas no efectuadas", flash[:warning]
  end
  
  test "should_response_si_hay_yumis_por_no_efectuadas" do
    HistYumi.create(type_hist_yumis:"Reserva no efectuada", quantity:-300, diner_id: @diner.id)
    get :yumis_reservas_noefectuadas
    assert_equal "Si hay yumis por reservas no efectuadas", flash[:success]
  end
 
  test "should_response_no_hay_yumis_por_canceladas" do
    get :yumis_reservas_canceladas
    assert_equal "Usted no tiene puntos yumis por reservas canceladas", flash[:warning]
  end
  
  test "should_response_si_hay_yumis_por_canceladas" do
    HistYumi.create(type_hist_yumis:"Reserva cancelada", quantity:-300, diner_id: @diner.id)
    get :yumis_reservas_canceladas
    assert_equal "Si hay yumis por reservas canceladas", flash[:success]
  end
 
end
