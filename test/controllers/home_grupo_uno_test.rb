require 'test_helper'

class HomeGrupoUnoControllerTest < ActionController::TestCase
include Devise::Test::ControllerHelpers

    def setup
          @controller = HomeController.new
    end

    test "Responde_correctamente_pagina_de_guia_rapida" do
        get :quick_user_guide 
        assert_template :quick_user_guide      
    end

    test "Validacion_pagina_no_existe" do
        get :quick_user_guide
        assert_not_empty :empty, "Error 404, No existe la página"
    end

end