require 'test_helper'

class ReservationsControllerGrupoCincoTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers

  def setup
    @controller=ReservationsController.new
    sign_in User.find(1)
  end
  
  
  #test para ver si hay reservaciones
  test "flash warning, busca si existen reservaciones" do
    get :index, params:{diner_id: 1}
    assert_equal 'No hay reservaciones registradas', flash[:warning]
    assert_not_nil assigns(:index)
  end  

end