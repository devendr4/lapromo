#Mi test
require 'test_helper'

class RestaurantGrupoTresControllerTest < ActionController::TestCase
  include Devise::Test::ControllerHelpers	

  def setup
    @controller = RestaurantsController.new
  end

  # test "Responder de manera correcta" do
  #   get 'buscador'
  #   assert_response :success
  # end

  test "Calificación inválida (superior a 5)" do 
    get 'calification_filter', params:{value:6, type:'service', state:25}
    assert_equal 'No se encontraron resultados para la búsqueda', flash[:error_ex]
  end

  test "Calificación inválida (inferior a 1)" do 
    get 'calification_filter', params:{value:-1, type:'ambience', state:25}
    assert_equal 'No se encontraron resultados para la búsqueda', flash[:error_ex]
  end

  test "Calificación inválida (Estado inválido)" do 
    get 'calification_filter', params:{value:3, type:'ambience', state:30}
    assert_equal 'El estado ingresado es inválido', flash[:error_state]
  end

  test "Calificación válida (Estado válido)" do 
    get 'calification_filter', params:{value:2, type:'kitchen', state:2}
    assert_equal 'El estado ingresado es válido', flash[:correct_state]
  end

  test "Calificación inválida (tipo erróneo)" do 
    get 'calification_filter', params:{value:2, type:'otro', state:2}
    assert_equal 'El tipo de búsqueda no es válida', flash[:error_type]
  end

  test "Calificación válida (tipo correcto)" do 
    get 'calification_filter', params:{value:2, type:'service', state:2}
    assert_equal 'El tipo de búsqueda es válida', flash[:correct_type]
  end

  

  # Pruebas de la historia h11
  # test "Correcto" do 
  #   get 'promos_date_filter', params:{ini:'01/01/2020', fin:'01/02/2020'}
  #   assert_response :success
  # end
  
  # test "Fecha inicio invalida" do 
  #   get 'promos_date_filter', params:{ini:'', fin:'01/01/2020'}
  #   assert_equal 'Debe establecer la fecha de inicio.', flash[:ini_error]
  # end

  # test "Fecha fin invalida" do 
  #   get 'promos_date_filter', params:{ini:'01/02/2020', fin:''}
  #   assert_equal 'Debe establecer la fecha de fin.', flash[:fin_error]
  # end

  # test "Fecha fin e inicio invalidas" do 
  #   get 'promos_date_filter', params:{ini:'01/02/2020', fin:'01/01/2020'}
  #   assert_equal 'La fecha de inicio NO puede ser mayor que la fecha de fin.', flash[:error]
  # end


  test "yummys filter prueba" do 
    get 'yummys_filter', params:{estado_id:'1112'}
    assert_response :redirect
    assert_not_nil assigns(:restaurants)
  end

  test "yummys filter prueba2" do 
    get 'yummys_filter', params:{estado_id:'efwef'}
    assert_response :redirect
    assert_not_nil assigns(:restaurants)
  end

  test "yummys filter prueba3" do 
    get 'yummys_filter', params:{}
    assert_response :redirect
    assert_not_nil assigns(:restaurants)
  end



  
   


end
