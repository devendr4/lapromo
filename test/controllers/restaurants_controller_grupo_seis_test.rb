require 'test_helper'

class RestaurantsControllerGrupoSeisTest < ActionController::TestCase

  include Devise::Test::ControllerHelpers

    def setup 
        @controller = RestaurantsController.new
    end

    test "Consultar restaurante por promocion" do
      r = Restaurant.joins(:promos).includes(:promos).where("promo_type = '2x1'")
      assert_not_empty r,"Lo sentimos, no se encontraron restaurantes en nuestro sistema bajo su criterio de búsqueda, Presione el botón para volver."
   end

    test "Consultar restaurante por tipo de comida" do
      r = Restaurant.joins(:restaurant_kitchens).includes(:restaurant_kitchens).where("kitchen_type_id = 501")
      assert_not_empty r,"Lo sentimos, no se encontraron restaurantes en nuestro sistema bajo su criterio de búsqueda, Presione el botón para volver."
    end

    test "Consultar restaurante por capacidad" do
      r = Restaurant.where("capacity = '10'")
     assert_not_empty r,"Lo sentimos, no se encontraron restaurantes en nuestro sistema bajo su criterio de búsqueda, Presione el botón para volver."
    end

#   test "Consultar restaurante por fecha y hora" do
#      r = Restaurant.where("start_time = '2020/01/01 10:00:00'")
#      assert_not_empty r,"Lo sentimos, no se encontraron restaurantes en nuestro sistema bajo su criterio de búsqueda, Presione el botón para volver."
#   end

end