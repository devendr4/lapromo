require 'test_helper'

class RestaurantsGrupoSeisControllerTest < ActionController::TestCase

    include Devise::Test::ControllerHelpers
  
    def setup 
      @controller = RestaurantsController.new
    end

    test "Mensaje de error en la vista del mapa cuando no existe restaurante" do
        get :mapa, params:{place_id: 5}
        assert_equal 'No hay restaurantes en este estado', flash[:warning]
    end


end