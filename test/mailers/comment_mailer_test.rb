require 'test_helper'

class CommentMailerTest < ActionMailer::TestCase

  test "validar new_recommendation_mail" do
    promo = create(:promo)
    diner = create(:diner)
    comment = create(:comment)
    correo = 'ferdy.rod23@gmail.com'
    email = CommentMailer.with(correo: correo, diner: diner, promo: promo, comment: comment).new_recommendation_mail
    assert_emails 1 do
      email.deliver_now
    end
    
    assert_equal ['lapromoucab2020@gmail.com'], email.from
    assert_equal ['ferdy.rod23@gmail.com'], email.to
    assert_equal ('Tu amigo ' + diner.name + ' te invita a aprovechar esta promosion'), email.subject
  end

end
