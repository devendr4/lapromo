require 'test_helper'
 
class InviteFriendsMailerTest < ActionMailer::TestCase
    
    test "Invitar amigo" do
       #falta crear un elemento para el setup
        email = InviteFriendsMailer.with(invite:@invite,reserva:@reserva).invite_friend_mail
        assert_emails 1 do
            email.deliver_now
        end
        
        assert_equal ['no-reply@lapromo.com'], email.from
        assert_equal [invite.mail], email.to
        assert_equal " Ha sido invitado al: " + reserva.restaurant.name , email.subject
    end
end