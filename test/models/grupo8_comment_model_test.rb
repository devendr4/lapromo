require 'test_helper'

class Grupo8CommentModelTest < ActiveSupport::TestCase

    test "kitchen_rating_is_required" do
        comment = build(:comment, kitchen_rating: nil)

        assert_not comment.valid?
        assert_not comment.errors[:kitchen_rating].empty?
    end

    test "kitchen_rating_must_be_numeric" do
        comment = build(:comment, kitchen_rating: 'not-a-number')
        
        assert_not comment.valid?
        assert_not comment.errors[:kitchen_rating].empty?
    end

    test "kitchen_rating_must_be_greater_than_or_equal_than_1" do
        comment = build(:comment, kitchen_rating: 0)
        
        assert_not comment.valid?
        assert_not comment.errors[:kitchen_rating].empty?

    end

    test "kitchen_rating_must_be_less_than_or_equal_than_5" do
        comment = build(:comment, kitchen_rating: 6)

        assert_not comment.valid?
        assert_not comment.errors[:kitchen_rating].empty?
    end

    test "kitchen_rating_can_have_decimals" do
        comment = create(:comment, kitchen_rating: 4.5)
        
        assert_equal 4.5, comment.kitchen_rating
    end

    test "service_rating_is_required" do
        comment = build(:comment, service_rating: nil)

        assert_not comment.valid?
        assert_not comment.errors[:service_rating].empty?
    end

    test "service_rating_must_be_numeric" do
        comment = build(:comment, service_rating: 'not-a-number')
        
        assert_not comment.valid?
        assert_not comment.errors[:service_rating].empty?
    end

    test "service_rating_must_be_greater_than_or_equal_than_1" do
        comment = build(:comment, service_rating: 0)
        
        assert_not comment.valid?
        assert_not comment.errors[:service_rating].empty?

    end

    test "service_rating_must_be_less_than_or_equal_than_5" do
        comment = build(:comment, service_rating: 6)
        
        assert_not comment.valid?
        assert_not comment.errors[:service_rating].empty?
    end

    test "service_rating_can_have_decimals" do
        comment = create(:comment, service_rating: 4.5)
        
        assert_equal 4.5, comment.service_rating
    end

    test "ambient_rating_is_required" do
        comment = build(:comment, ambient_rating: nil)

        assert_not comment.valid?
        assert_not comment.errors[:ambient_rating].empty?
    end

    test "ambient_rating_must_be_numeric" do
        comment = build(:comment, ambient_rating: 'not-a-number')
        
        assert_not comment.valid?
        assert_not comment.errors[:ambient_rating].empty?
    end

    test "ambient_rating_must_be_greater_than_or_equal_than_1" do
        comment = build(:comment, ambient_rating: 0)
        
        assert_not comment.valid?
        assert_not comment.errors[:ambient_rating].empty?

    end

    test "ambient_rating_must_be_less_than_or_equal_than_5" do
        comment = build(:comment, ambient_rating: 6)
        
        assert_not comment.valid?
        assert_not comment.errors[:ambient_rating].empty?
    end

    test "ambient_rating_can_have_decimals" do
        comment = build(:comment, ambient_rating: 4.5)
        
        assert_equal 4.5, comment.ambient_rating
    end

    test "description_is_required" do
        comment = build(:comment, description: nil, attached_photos: [])

        assert_not comment.valid?
        assert_not comment.errors[:description].empty?
    end
end